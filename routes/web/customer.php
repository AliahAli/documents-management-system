<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\MeetingController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'customer', 'middleware' => ['auth']], function () {
    Route::get('index', [CustomerController::class, 'index'])->name('customer.index');

    Route::group(['prefix' => 'create'], function () {
        Route::get('', [CustomerController::class, 'create'])
            ->name('customer.create');

        Route::post('', [CustomerController::class, 'store'])
            ->name('customer.store');
    });

    Route::group(['prefix' => '{customer}'], function () {

        Route::get('overview', [CustomerController::class, 'overview'])
            ->name('customer.overview');

        Route::group(['prefix' => 'meeting'], function () {
            Route::get('', [MeetingController::class, 'create'])
                ->name('meeting.create');

            Route::post('', [MeetingController::class, 'store'])
                ->name('meeting.store');

            Route::group(['prefix' => '{meeting}'], function () {
                Route::group(['prefix' => 'edit'], function () {
                    Route::get('', [MeetingController::class, 'edit'])
                        ->name('meeting.edit');

                    Route::post('', [MeetingController::class, 'update'])
                        ->name('meeting.update');
                });

                Route::get('view', [MeetingController::class, 'view'])
                    ->name('meeting.view');

                Route::get('delete', [MeetingController::class, 'delete'])
                    ->name('meeting.delete');
            });
        });

        Route::group(['prefix' => 'document'], function () {
            Route::get('', [DocumentController::class, 'upload'])
                ->name('document.upload');

            Route::post('', [DocumentController::class, 'store'])
                ->name('document.store');
        });

        Route::group(['prefix' => 'edit'], function () {
            Route::get('', [CustomerController::class, 'edit'])
                ->name('customer.edit');

            Route::post('', [CustomerController::class, 'update'])
                ->name('customer.update');
        });

        Route::get('delete', [CustomerController::class, 'delete'])
            ->name('customer.delete');
    });
});
