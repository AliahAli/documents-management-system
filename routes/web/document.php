<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DocumentController;

Route::group(['prefix' => 'document', 'middleware' => ['auth', 'web']], function () {

    Route::get('index', [DocumentController::class, 'index'])->name('document.index');

    Route::group(['prefix' => '{folder}'], function () {

        Route::get('', [DocumentController::class, 'viewMyFolder'])
            ->name('folder.view');
    });

    Route::group(['prefix' => '{document}'], function () {

        Route::get('', [DocumentController::class, 'view'])
            ->name('document.view');
    });

});
