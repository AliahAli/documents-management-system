<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SettingsController;

Route::group(['prefix' => 'footer','middleware' => ['auth']], function () {

    Route::group(['prefix' => 'edit'], function () {
        Route::get('', [SettingsController::class, 'editFooter'])
            ->name('footer.edit');

        Route::post('', [SettingsController::class, 'updateFooter'])
            ->name('footer.update');
    });
});

Route::group(['prefix' => 'login','middleware' => ['auth']], function () {

    Route::group(['prefix' => 'edit'], function () {
        Route::get('', [SettingsController::class, 'editLogin'])
            ->name('login.edit');

        Route::post('', [SettingsController::class, 'updateLogin'])
            ->name('login.update');
    });
});

Route::group(['prefix' => 'sidebar','middleware' => ['auth']], function () {

    Route::group(['prefix' => 'edit'], function () {
        Route::get('', [SettingsController::class, 'editSidebar'])
            ->name('sidebar.edit');

        Route::post('', [SettingsController::class, 'updateSidebar'])
            ->name('sidebar.update');
    });
});