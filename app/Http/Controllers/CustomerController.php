<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerContact;
use App\Models\CustomerActivity;
use App\Models\CustomerMeeting;
use App\Models\User;
use App\Notifications\NewCustomerNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'customer-index'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "system/customer/index", 'name' => "Customer List"],
        ];

        return view('system/customer/customer-index', compact('pageConfigs', 'breadcrumbs', 'customers', 'notifications'));
    }

    public function create()
    {
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'customer-create'];

        $breadcrumbs = [
            ['link' => "system/customer/index", 'name' => "Customer List"], ['link' => "javascript:void(0)", 'name' => "Create Customer"]
        ];

        return view('system/customer/customer-create', compact('pageConfigs', 'breadcrumbs', 'notifications'));
    }

    public function store(Request $request, User $user)
    {
        $user = User::all();

        $customers = new Customer();
        $customers->company_name = $request->company_name;
        $customers->registration_no = $request->registration_no;
        $customers->address1 = $request->address1;
        $customers->address2 = $request->address2;
        $customers->postcode = $request->postcode;
        $customers->state = $request->state;
        $customers->city = $request->city;
        $customers->website = $request->website;
        $customers->phone = $request->phone;
        $customers->email = $request->email;
        $customers->notes = $request->notes;
        $customers->save();

        foreach ($request->input('contacts', []) as $contact) {
            $customers->contact()->create([
                'name' => $contact['name'],
                'phone' => $contact['phone'],
                'email' => $contact['email'],
            ]);
            $activity = new CustomerActivity();
            $activity->customer_id = $customers->id;
            $activity->activity = $contact['name'] . CustomerActivity::ACTIVITY_CREATE_USER . auth()->user()->name;
            $activity->save();
        }

        Notification::sendNow($user, new NewCustomerNotification($customers));

        return redirect()->route('customer.index');
    }

    public function edit($id)
    {
        $customer = Customer::where('id', $id)->first();
        $contact = CustomerContact::all();
        $activity = CustomerActivity::all();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'customer-edit'];

        $breadcrumbs = [
            ['link' => "system/customer/index", 'name' => "Customer List"], ['link' => "javascript:void(0)", 'name' => "Edit Customer"]
        ];

        return view('system/customer/customer-edit', compact('pageConfigs', 'breadcrumbs', 'customer', 'contact', 'notifications'));
    }

    public function update(Customer $customer, Request $request)
    {
        $customer->company_name = $request->company_name;
        $customer->registration_no = $request->registration_no;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->postcode = $request->postcode;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->website = $request->website;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->notes = $request->notes;
        $customer->save();

        $customer->contact()->whereNotIn('id', $request->input('contacts.*.id', []))
            ->delete();

        foreach ($request->input('contacts', []) as $contact) {
            if (@$contact['id']) {
                $customer->contact()->find($contact['id'])->update([
                    'name' => $contact['name'],
                    'phone' => $contact['phone'],
                    'email' => $contact['email'],
                ]);

                $activity = CustomerActivity::find($contact['id']);
                $activity->activity = $customer->id;
                $activity->activity = $contact['name'] . CustomerActivity::ACTIVITY_UPDATE_USER . auth()->user()->name;
                $activity->save();
            } else {
                $customer->contact()->create([
                    'name' => $contact['name'],
                    'phone' => $contact['phone'],
                    'email' => $contact['email'],
                ]);
                $activity = new CustomerActivity();
                $activity->customer_id = $customer->id;
                $activity->activity = $contact['name'] . CustomerActivity::ACTIVITY_CREATE_USER . auth()->user()->name;
                $activity->save();
            }
        }
        return redirect()->route('customer.index');
    }

    public function overview($id)
    {
        $customer = Customer::where('id', $id)->first();
        $activities = CustomerActivity::all();
        $meetings = CustomerMeeting::all();
        $user = Auth::user()->id;
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'customer-overview'];

        $breadcrumbs = [
            ['link' => "system/customer/index", 'name' => "Customer List"], ['link' => "javascript:void(0)", 'name' => "Customer Overview"]
        ];

        return view('system/customer/customer-overview', compact('pageConfigs', 'breadcrumbs', 'activities', 'customer', 'meetings', 'notifications'));
    }

    public function delete(Customer $customer)
    {
        $customer->delete();
        return redirect()->route('customer.index');
    }
}
