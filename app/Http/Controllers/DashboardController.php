<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerActivity;
use App\Models\CustomerMeeting;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  // Dashboard - Analytics
  public function dashboardAnalytics()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-analytics', ['pageConfigs' => $pageConfigs]);
  }

  // Dashboard - Ecommerce
  public function dashboardEcommerce()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-ecommerce', ['pageConfigs' => $pageConfigs]);
  }

  // Dashboard - Document Management System
  public function dashboardDocumentManagement(Customer $customer)
  {
    $meetings = CustomerMeeting::all();
    $activities = CustomerActivity::all();
    $today = Carbon::today();
    $tomorrow = Carbon::tomorrow();
    $notifications = auth()->user()->unreadNotifications ?? [];

    $todayMeetings = CustomerMeeting::whereDate('meeting_at', $today)->get();
    $tomorrowMeetings = CustomerMeeting::whereDate('meeting_at', $tomorrow)->get();

    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-documents-management', compact('pageConfigs', 'meetings', 'customer', 'today', 'tomorrow', 'todayMeetings', 'tomorrowMeetings', 'notifications', 'activities'));
  }
}
