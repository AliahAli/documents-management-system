<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerActivity;
use App\Models\CustomerMeeting;
use App\Models\CustomerMeetingDocument;
use App\Models\DocumentTagging;
use App\Models\User;
use App\Notifications\NewMeetingNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class MeetingController extends Controller
{
    public function create(Customer $customer)
    {
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'meeting-create'];

        $breadcrumbs = [
            ['link' => route('customer.overview', $customer), 'name' => "Customer Overview"], ['link' => "javascript:void(0)", 'name' => "Create Meeting"]
        ];

        return view('system/meeting/meeting-create', compact('pageConfigs', 'breadcrumbs', 'notifications', 'customer'));
    }

    public function store(Request $request, Customer $customer)
    {
        $user = User::all();

        $meetings = new CustomerMeeting();
        $meetings->location = $request->location;
        $meetings->meeting_update = $request->meeting_update;
        $meetings->member = $request->member;
        $meetings->meeting_at = $request->meeting_at;
        $meetings->user_id =  $request->user()->id;
        $meetings->customer_id = $customer->id;
        $meetings->save();

        foreach ($request->input('documents', []) as $key => $document) {
            $file = $request->file('documents.' . $key . '.filename');

            $doc = new CustomerMeetingDocument();
            $doc->title = $document['title'];
            $doc->description = $document['description'];
            $doc->filename = time() . '_' . $file->getClientOriginalName();
            $doc->meeting_id = $meetings->id;
            $doc->save();

            $metatag = new DocumentTagging();
            $metatag->tag = $document['tag'];
            $metatag->document_id = $doc->id;
            $metatag->save();

            $filename = $file->getClientOriginalName();
            $file->move(public_path() . '/uploads/', $filename);
        }

        $activity = new CustomerActivity();
        $activity->customer_id = $customer->id;
        $activity->activity = $meetings->location . CustomerActivity::ACTIVITY_CREATE_MEETING . auth()->user()->name;
        $activity->save();

        Notification::sendNow($user, new NewMeetingNotification($meetings));

        return redirect()->route('customer.overview', $customer);
    }

    public function edit(Customer $customer, CustomerMeeting $meeting)
    {
        $activity = CustomerActivity::all();
        $document = CustomerMeetingDocument::all();
        $metatag = DocumentTagging::all();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'meeting-edit'];

        $breadcrumbs = [
            ['link' => route('customer.overview', $customer), 'name' => "Customer Overview"], ['link' => "javascript:void(0)", 'name' => "Edit Meeting"]
        ];

        return view('system/meeting/meeting-edit', compact('pageConfigs', 'breadcrumbs', 'notifications', 'document', 'metatag', 'customer', 'meeting'));
    }

    public function update(Request $request, Customer $customer, CustomerMeeting $meeting, DocumentTagging $metatag, CustomerMeetingDocument $doc)
    {
        $meeting->location = $request->location;
        $meeting->meeting_update = $request->meeting_update;
        $meeting->member = $request->member;
        $meeting->meeting_at = $request->meeting_at;
        $meeting->update_at;
        $meeting->customer_id = $customer->id;
        $meeting->save();

        $meeting->documents()->whereNotIn('id', $request->input('documents.*.id', []))
            ->delete();

        foreach ($request->input('documents', []) as $key => $document) {
            $file = $request->file('documents.' . $key . '.filename');

            if (@$document['id']) {
                if ($file) {
                    $doc->title = $document['title'];
                    $doc->description = $document['description'];
                    $doc->filename = time() . '_' . $file->getClientOriginalName();
                    $doc->meeting_id = $meeting->id;
                    $doc->save();

                    $metatag->tag = $document['tag'];
                    $metatag->document_id = $doc->id;
                    $metatag->save();

                    $filename = $file->getClientOriginalName();
                    $file->move(public_path() . '/uploads/', $filename);
                } else {
                    $doc->title = $document['title'];
                    $doc->description = $document['description'];
                    $doc->meeting_id = $meeting->id;
                    $doc->save();

                    $metatag->tag = $document['tag'];
                    $metatag->document_id = $doc->id;
                    $metatag->save();
                }
                $activity = new CustomerActivity();
                $activity->customer_id = $customer->id;
                $activity->activity = $meeting->location . CustomerActivity::ACTIVITY_UPDATE_MEETING . auth()->user()->name;
                $activity->save();
            } else {
                $doc = new CustomerMeetingDocument();
                $doc->title = $document['title'];
                $doc->description = $document['description'];
                $doc->filename = time() . '_' . $file->getClientOriginalName();
                $doc->meeting_id = $meeting->id;
                $doc->save();

                $metatag = new DocumentTagging();
                $metatag->tag = $document['tag'];
                $metatag->document_id = $doc->id;
                $metatag->save();

                $filename = $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/', $filename);
            }

            $activity = new CustomerActivity();
            $activity->customer_id = $customer->id;
            $activity->activity = $meeting->location . CustomerActivity::ACTIVITY_CREATE_MEETING . auth()->user()->name;
            $activity->save();
        }

        return redirect()->route('customer.overview', $customer);
    }

    public function view(Customer $customer, CustomerMeeting $meeting)
    {
        $activity = CustomerActivity::all();
        $documents = CustomerMeetingDocument::all();
        $metatags = DocumentTagging::all();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'meeting-edit'];

        $breadcrumbs = [
            ['link' => route('customer.overview', $customer), 'name' => "Customer Overview"], ['link' => "javascript:void(0)", 'name' => "View Meeting"]
        ];

        return view('system/meeting/meeting-view', compact('pageConfigs', 'breadcrumbs', 'notifications', 'documents', 'metatags', 'customer', 'meeting'));
    }

    public function delete(Customer $customer, CustomerMeeting $meeting)
    {
        $meeting->delete();
        return redirect()->route('customer.overview', $customer);
    }
}
