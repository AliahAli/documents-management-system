<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function editFooter()
    {
        $footer = Setting::where('name', 'footer')->first();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'footer-edit'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "javascript:void(0)", 'name' => "Footer"],
        ];

        return view('system/setting/footer-edit', compact('pageConfigs', 'breadcrumbs', 'notifications', 'footer'));
    }

    public function updateFooter(Request $request)
    {
        $footer = Setting::where('name', 'footer')->first();
        $footer->name = $request->name;
        $footer->value = $request->value;
        $footer->save();

        return redirect()->route('footer.edit');
    }

    public function editLogin()
    {
        $login = Setting::where('name', 'login')->first();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'footer-edit'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "javascript:void(0)", 'name' => "Login"],
        ];

        return view('system/setting/login-edit', compact('pageConfigs', 'breadcrumbs', 'notifications', 'login'));
    }

    public function updateLogin(Request $request)
    {
        $login = Setting::where('name', 'login')->first();
        $login->name = $request->name;
        $login->value = $request->value;
        $login->image = $request->image;

        if ($request->hasfile('path')) {
            $file = $request->file('path');
            $filename = time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/login', $filename);
            $login->path = $filename;
        }
        $login->save();

        return redirect()->route('login.edit');
    }

    public function editSidebar()
    {
        $sidebar = Setting::where('name', 'sidebar')->first();
        $notifications = auth()->user()->unreadNotifications ?? [];

        $pageConfigs = ['pageClass' => 'footer-edit'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "javascript:void(0)", 'name' => "Sidebar"],
        ];

        return view('system/setting/sidebar-edit', compact('pageConfigs', 'breadcrumbs', 'notifications', 'sidebar'));
    }

    public function updateSidebar(Request $request)
    {
        $sidebar = Setting::where('name', 'sidebar')->first();
        $sidebar->name = $request->name;
        $sidebar->value = $request->value;
        $sidebar->image = $request->image;

        if ($request->hasfile('path')) {
            $file = $request->file('path');
            $filename = time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/sidebar', $filename);
            $sidebar->path = $filename;
        }
        $sidebar->save();

        return redirect()->route('sidebar.edit');
    }
}
