<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerActivity;
use App\Models\CustomerDocument;
use App\Notifications\NewDocumentNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
use App\Models\UserFolder;

class DocumentController extends Controller
{
    public function index(User $user)
    {
        $customers = Customer::all();
        $folders = UserFolder::all();

        $pageConfigs = ['pageClass' => 'document-index'];
        $notifications = auth()->user()->unreadNotifications ?? [];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "system/document/index", 'name' => "Document"],
        ];

        return view('system/document/document-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'notifications' => $notifications,
            'customers' => $customers,
            'folders' => $folders,
            'user' => $user
        ]);
    }

    public function createFolder()
    {

        return view('content/_partials/_modals/modal-add-new-cc');
    }

    public function storeFolder(Request $request, User $user)
    {

        $folders = new UserFolder();
        $folders->folderName = $request->folderName;
        $folders->user_id = $user->id;
        $folders->save();

        return redirect()->route('document-index');
    }

    public function viewMyFolder($id)
    {
        $folders = UserFolder::where('id', $id)->first();

        $pageConfigs = ['pageClass' => 'document-index'];
        $notifications = auth()->user()->unreadNotifications ?? [];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "system/document/index", 'name' => "$folders->folderName"],
        ];

        return view('system/document/document-view-myFolder', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'notifications' => $notifications,
            'folders' => $folders
        ]);
    }

    public function upload(Customer $customer)
    {
        $pageConfigs = ['pageClass' => 'meeting-create'];
        $notifications = auth()->user()->unreadNotifications ?? [];

        $breadcrumbs = [
            ['link' => route('customer.overview', $customer), 'name' => "Customer Overview"], ['link' => "javascript:void(0)", 'name' => "Upload Document"]
        ];

        return view('system/customer/customer-document', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'customer' => $customer,
            'notifications' => $notifications
        ]);
    }

    public function store(Request $request, Customer $customer)
    {
        $user = User::all();

        $documents = new CustomerDocument();
        $documents->customer_id = $customer->id;
        $documents->title = $request->title;
        $documents->description = $request->description;
        $documents->meta_tags = $request->meta_tags;

        if ($request->file()) {
            $documentName = time() . '_' . $request->file->getClientOriginalName();
            $documentPath = $request->file('file')->storeAs('uploads', $documentName, 'public');

            $documents->filename = time() . '_' . $request->file->getClientOriginalName();
            $documents->path = '/storage/' . $documentPath;
            $documents->save();

            Notification::sendNow($user, new NewDocumentNotification($documents));

            return redirect()->route('customer.overview', $customer);
        }
        $activity = new CustomerActivity();
        $activity->customer_id = $customer->id;
        $activity->activity = $documents->title . CustomerActivity::ACTIVITY_CREATE_CUSTOMER_DOCUMENT  . auth()->user()->name;
        $activity->save();
    }

    public function view($id)
    {
        $customer = Customer::where('id', $id)->first();
        $documents = CustomerDocument::all();

        $pageConfigs = ['pageClass' => 'document-index'];
        $notifications = auth()->user()->unreadNotifications ?? [];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "system/document/index", 'name' => "Document"],
            ['link' => "javascript:void(0)", 'name' => "View Document"]
        ];

        return view('system/document/document-view', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'notifications' => $notifications,
            'customer' => $customer,
            'documents' => $documents
        ]);
    }
}
