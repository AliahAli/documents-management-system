<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('panels/footer', function($view){
            $view->with('footer', Setting::where('name', 'footer')->first());
        });

        view()->composer('auth/login', function($view){
            $view->with('login', Setting::where('name', 'login')->first());
        });
        view()->composer('panels/sidebar', function($view){
            $view->with('sidebar', Setting::where('name', 'sidebar')->first());
        });
    }

}
