<?php

namespace App\Providers;

use App\Models\User;
use App\Notifications\NewCustomerNotification;
use App\Notifications\NewMeetingNotification;
use App\Notifications\NewDocumentNotification;
use App\Providers\CreateNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\CreateNotification  $event
     * @return void
     */
    public function handle(CreateNotification $event)
    {
        $user = User::all();

        Notification::send($user, new NewCustomerNotification($event->customers));
        Notification::send($user, new NewMeetingNotification($event->meetings));
        Notification::send($user, new NewDocumentNotification($event->documents));

    }
}
