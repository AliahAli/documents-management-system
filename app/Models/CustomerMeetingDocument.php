<?php

namespace App\Models;

use CreateDocumentTaggingsTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerMeetingDocument extends Model
{
    use HasFactory;

    protected $fillable = ['meeting_id', 'filename', 'title', 'meta_tags', 'description'];

    public function meeting()
    {
        return $this->belongsTo(CustomerMeeting::class, 'meeting_id');
    }

    public function tags()
    {
        return $this->hasMany(DocumentTagging::class, 'document_id');
    } 
}
