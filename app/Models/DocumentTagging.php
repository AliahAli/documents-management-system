<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentTagging extends Model
{
    use HasFactory;

    protected $fillable = ['document_id','tag'];

    public function meetingDocument()
    {
        return $this->belongsTo(CustomerMeetingDocument::class,'document_id');
    } 
}
