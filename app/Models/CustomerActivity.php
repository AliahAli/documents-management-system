<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerActivity extends Model
{

    const ACTIVITY_CREATE_USER = ' had been created by ';

    const ACTIVITY_UPDATE_USER = ' had been updated by ';

    const ACTIVITY_CREATE_MEETING = ' meeting report had been posted by ';

    const ACTIVITY_UPDATE_MEETING = ' meeting report had been updated by ';

    const ACTIVITY_CREATE_CUSTOMER_DOCUMENT = ' document had been posted by ';

    const ACTIVITY_UPDATE_CUSTOMER_DOCUMENT = ' document had been updated by ';

    use HasFactory;

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
