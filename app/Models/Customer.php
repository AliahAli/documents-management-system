<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use SoftDeletes, HasFactory, Notifiable;

    protected $fillable = [
        'running_number',
        'year',
        'company_name',
        'registration_no',
        'address1',
        'address2',
        'postcode',
        'city',
        'state',
        'website',
        'phone',
        'email',
        'notes',
    ];

    protected $appends = ['display_id'];

    public function contact()
    {
        return $this->hasMany(CustomerContact::class, 'customer_id');
    }

    public function activities()
    {
        return $this->hasMany(CustomerActivity::class, 'customer_id')->latest();
    }

    public function meetings()
    {
        return $this->hasMany(CustomerMeeting::class, 'customer_id')->latest();
    }

    public function documents()
    {
        return $this->hasMany(CustomerDocument::class, 'customer_id');
    }

    public function getDisplayIdAttribute()
    {
        return 'CST' . (1000 + $this->id) . '-' . date('Y');
    }

}
