<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CustomerDocument extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['customer_id', 'path', 'filename', 'title', 'meta_tags', 'description'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
