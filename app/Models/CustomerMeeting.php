<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class CustomerMeeting extends Model
{
    use SoftDeletes, HasFactory, Notifiable;

    protected $fillable = ['customer_id', 'location', 'member', 'meeting_at', 'meeting_update', 'user_id'];

    protected $dates = [ 'meeting_at' ]; 

    protected $with = ['customer'];

    protected $appends = ['user_name'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function documents()
    {
        return $this->hasMany(CustomerMeetingDocument::class, 'meeting_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getCustomerNameAttribute()
    {
        return $this->customer->company_name;
    }

    public function getUserNameAttribute()
    {
        return $this->user->name;
    }
}
