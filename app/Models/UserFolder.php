<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFolder extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'folderName'];

    public function folders()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


}
