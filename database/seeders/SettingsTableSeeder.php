<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::insert(
            array(

                0 =>
                array(
                    'id' => 1,
                    'name' => 'footer',
                    'value' => 'All rights Reserved',
                    'image' => 'none',
                    'path' => 'none',
                ),

                1 =>
                array(
                    'id' => 2,
                    'name' => 'login',
                    'value' => 'Vuexy! 👋',
                    'image' => 'Vuexy',
                    'path' => 'logo.png',
                ),

                3 =>
                array(
                    'id' => 3,
                    'name' => 'sidebar',
                    'value' => '-',
                    'image' => 'Vuexy',
                    'path' => 'logo.png',
                ),
            )
        );
    }
}
