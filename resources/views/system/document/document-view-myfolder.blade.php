@extends('layouts/contentLayoutMaster')

@section('title', 'View Document')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')
<section>
    <div class="card">
        <div class="border-bottom">
            <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                <div class="me-1">
                    <h4 class="card-title">{{ $folders->folderName }}</h4>
                </div>
                <a class="btn btn-primary" href="{{ route('customer.create') }}">
                    <i data-feather="user-plus" class="align-middle me-sm-25 me-0"></i>
                    <span>Add File</span>
                </a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body px-2 py-2 align-items-center">
            <div class="card-body">
                <div class="row">
                    {{-- @foreach ( $customer->documents as $document) --}}
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="card">
                            <object data="" type="application/pdf" width="100%" height="100%" frameborder="0"></object>
                            <p class="card-text text-center"></p>
                        </div>
                    </div>
                    {{-- @endforeach --}}
                    {{-- @if (count($customer->documents) == 0) --}}
                    <div class="col-12">
                        <p class="card-text text-center">No document</p>
                    </div>
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection
