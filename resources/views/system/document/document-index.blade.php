@extends('layouts/contentLayoutMaster')

@section('title', 'Document')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/modal-create-app.css')) }}">
@endsection

@section('content')
<section>
    <div class="card">
        <div class="border-bottom">
            <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                <div class="me-1">
                    <h4 class="card-title">My folder</h4>
                </div>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addNewCard">
                    Create Folder
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach ( auth()->user()->folders as $folder )
        <div class="col-lg-3 col-sm-6 col-12">
            <a href="{{ route('folder.view',$folder->id) }}">
                <div class="card">
                    <div class="card-header">
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="folder" class="font-medium-5"></i>
                            </div>
                        </div>
                        <div>
                            <h4 class="fw-bolder">{{ $folder->folderName }}</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Customers Folder</span></h4>
        </div>
    </div>
    <div class="row">
        @foreach ( $customers as $customer )
        <div class="col-lg-3 col-sm-6 col-12">
            <a href="{{ route('document.view',$customer->id) }}">
                <div class="card">
                    <div class="card-header">
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="folder" class="font-medium-5"></i>
                            </div>
                        </div>
                        <div>
                            <h4 class="fw-bolder">{{ $customer->display_id }}</h4>
                        </div>
                    </div>
                    <div class="mb-2 text-center">
                        <p class="card-text">{{ $customer->company_name }}</p>
                    </div>

                </div>
            </a>
        </div>
        @endforeach
    </div>
</section>
@include('content/_partials/_modals/modal-add-new-cc')
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-add-new-cc.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/page-pricing.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-add-new-address.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-create-app.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-two-factor-auth.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
     <script src="{{ asset(mix('js/scripts/pages/modal-share-project.js')) }}"></script>
@endsection