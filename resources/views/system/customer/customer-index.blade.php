@extends('layouts/contentLayoutMaster')

@section('title', 'Customer List')

@section('vendor-style')
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')

<section>
    <div class="card">
        <div class="border-bottom">
            <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                <div class="me-1">
                    <h4 class="card-title">List of Customers</h4>
                </div>
                <a class="btn btn-primary" href="{{ route('customer.create') }}">
                    <i data-feather="user-plus" class="align-middle me-sm-25 me-0"></i>
                    <span>Add Customer</span>
                </a>
            </div>
        </div>
        <div class="card-datatable table-responsive px-2 py-2">
            <table class="invoice-list-table table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company Name</th>
                        <th>Registration Number</th>
                        <th class="cell-fit">Action</th>
                    </tr>
                    @foreach($customers as $customer)
                    <tr class="align-top">
                        <td>{{ $customer->display_id }}</td>
                        <td>{{ $customer->company_name }}</td>
                        <td>{{ $customer->registration_no }}</td>
                        <td>
                            <div class="d-inline-flex align-items-center">
                                <a class="me-1" href="{{ route('customer.overview',$customer->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Overview">
                                    <i data-feather="eye" class="font-medium-3"></i>
                                </a>
                                <a class="me-1" href="{{ route('customer.edit',$customer->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                    <i data-feather="edit" class="font-medium-3"></i>
                                </a>
                                <a href="{{ route('customer.delete',$customer->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete">
                                    <i data-feather="trash-2" class="font-medium-3"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    @if (count($customers) == 0)
                    <tr>
                        <td class="border-t px-6 py-4 text-center" colspan="6">No data found</td>
                    </tr>
                    @endif
                </thead>
            </table>
        </div>
    </div>
</section>
@endsection

@section('vendor-script')
    <script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection