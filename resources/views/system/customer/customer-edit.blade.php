@extends('layouts/contentLayoutMaster')

@section('title', 'Edit Customer')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')
<section>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit <span>{{ $customer->company_name}}</span></span></h4>
        </div>
    </div>
    <div class="card bg-light-primary">
        <ul class="list-unstyled px-2 py-2">
            <li>
                <span class="fw-bolder">Company Name: </span>
                <span> {{ $customer->company_name}}</span>
            </li>
            <li class="mt-1">
                <span class="fw-bolder">Address: </span>
                <span> {{$customer->address1}} , {{$customer->address1}} , {{$customer->postcode}} , {{$customer->city}} , {{$customer->state}} </span>
            </li>
            <li class="mt-1">
                <span class="fw-bolder">Website: </span>
                <span> {{$customer->website}}</span>
            </li>
        </ul>
    </div>
    <div class="card">
        <div class="card-body px-2 py-2 align-items-center">
            <form action="{{ route('customer.update', $customer->id)}}" method="POST" class="customer repeater-default">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for="company_name">Company Name :</h4>
                                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{ $customer->company_name}}" required />
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for="registration_no">Registration Number :</h4>
                                    <input type="number" class="form-control" id="registration_no" name="registration_no" placeholder="Registration Number" value="{{$customer->registration_no}}" placeholder="Insert number only..." required />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for=" ">Address:</h4>
                                    <input type="text" class="form-control" id="address1" name="address1" value="{{$customer->address1}}" required />
                                    <input type="text" class="mt-1 form-control" id="address2" name="address2" value="{{$customer->address2}}" />
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for=" ">Postcode :</h4>
                                    <input type="number" class="form-control" id="postcode" name="postcode" value="{{$customer->postcode}}" placeholder="Insert number only" required />
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1 mb-md-0">
                                <h4 class="card-title" for=" ">City :</h4>
                                <input type="text" class="form-control" id="city" name="city" value="{{$customer->city}}" required />
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1 mb-md-0">
                                <h4 class="card-title" for=" ">State :</h4>
                                <input type="text" class="form-control" id="state" name="state" value="{{$customer->state}}" required />
                            </div>
                            <div class="col-xl-6 col-md-6 col-12">
                                <h4 class="card-title" for="website">Website :</h4>
                                <input type="url" class="form-control" id="website" name="website" value="{{$customer->website}}" placeholder="Insert url only" required />
                            </div>
                        </div>
                        <!-- Invoice repeater -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Person in Charge</h4>
                                </div>
                                <div class="card-body">
                                    <div data-repeater-list="contacts">
                                        @foreach ( $customer->contact as $key => $contacts)
                                        <div data-repeater-item>
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2">
                                                        <input value="{{ $contacts->id }}" type="hidden" class="form-control" id="id[]" name="contacts[{{ $key }}][id]" required />
                                                        <label class="form-label" for="name">Name</label>
                                                        <input value="{{ $contacts->name }}" type="text" class="form-control" id="name" name="contacts[{{ $key }}][name]" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2">
                                                        <label class="form-label" for="phone">Phone</label>
                                                        <input type="number" class="form-control" id="phone" name="contacts[{{ $key }}][phone]" value="{{$contacts->phone}}" placeholder="0123456789" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2">
                                                        <label class="form-label" for="email">Email</label>
                                                        <input type="email" class="form-control" id="email" name="contacts[{{ $key }}][email]" value="{{$contacts->email}}" placeholder="mail@example.com" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12 mb-50">
                                                    <div class="mb-1">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="me-25"></i>
                                                            <span>Delete</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="me-25"></i>
                                                <span>Add New</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Invoice repeater -->
                        <div class="col-12">
                            <h4 class="card-title" for="website">Notes :</h4>
                            <textarea class="form-control" rows="5" id="notes" name="notes">{{ old('notes', $customer->notes) }}</textarea>
                        </div>
                        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                            <div class="me-1">
                                <a class="btn btn-outline-secondary" href="{{ route('customer.index') }}">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span>Back</span>
                                </a>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection