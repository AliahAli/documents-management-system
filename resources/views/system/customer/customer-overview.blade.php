@extends('layouts/contentLayoutMaster')

@section('title', 'Customer Overview')

@section('vendor-style')
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section class="app-user-view-account">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Overview of <span>{{ $customer->company_name}}</span></h4>
        </div>
    </div>

    <div class="row">
        <!-- User Sidebar -->
        <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
            <!-- User Card -->
            <div class="card">
                <div class="border-bottom">
                    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                        <div class="me-1">
                            <h4 class="card-title">Activities Timeline</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="timeline">
                        @foreach ($customer->activities as $row)
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{ $row->activity}}</h6>
                                    <span class="timeline-event-time">{{ $row->updated_at ->diffForHumans()}}</span>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- /User Card -->
        </div>
        <!--/ User Sidebar -->
        <!-- User Content -->
        <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
            <!-- Project table -->
            <div class="card">
                <div class="border-bottom">
                    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                        <div class="me-1">
                            <h4 class="card-title">Meetings</h4>
                        </div>
                        <div class="d-inline-flex align-items-center">
                            <a class="btn btn-primary me-1" href="{{ route('document.upload', $customer) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Upload Document">
                                <i data-feather='upload' class="align-middle me-sm-25 me-0"></i>
                            </a>
                            <a class="btn btn-primary" href="{{ route('meeting.create', $customer) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Post Meeting Report">
                                <i data-feather='file-text' class="align-middle me-sm-25 me-0"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-datatable table-responsive px-2 py-2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Location</th>
                                <th>User</th>
                                <th class="cell-fit">Action</th>
                            </tr>
                            @foreach($customer->meetings as $meeting)
                            <tr class="align-top">
                                <td>{{ Carbon\Carbon::parse($meeting->meeting_at)->format('D- d M Y') }}</td>
                                <td>{{ $meeting->location }}</td>
                                <td>{{ $meeting->user_name}}</td>
                                <td>
                                    <div class="d-inline-flex align-items-center">
                                        @if ($meeting->user_id == auth()->user()->id)
                                        <a class="me-1" href="{{ route('meeting.view', ['customer' => $customer, 'meeting' => $meeting]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="View Meeting">
                                            <i data-feather="eye" class="font-medium-3"></i>
                                        </a>
                                        <a class="me-1" href="{{ route('meeting.edit', ['customer' => $customer, 'meeting' => $meeting]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit Meeting">
                                            <i data-feather="edit" class="font-medium-3"></i>
                                        </a>
                                        @else
                                        <a class="me-1" href="{{ route('meeting.edit', ['customer' => $customer, 'meeting' => $meeting]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="View Meeting">
                                            <i data-feather="eye" class="font-medium-3"></i>
                                        </a>
                                        <a class="me-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Only {{ $meeting->user_name }} can edit">
                                            <i data-feather="edit" class="font-medium-3"></i>
                                        </a>
                                        @endif
                                        <a href="{{ route('meeting.delete', ['customer' => $customer, 'meeting' => $meeting]) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete">
                                            <i data-feather="trash-2" class="font-medium-3"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @if (count($customer->meetings) == 0)
                            <tr>
                                <td class="border-t px-6 py-4 text-center" colspan="6">You have no meeting</td>
                            </tr>
                            @endif
                        </thead>
                    </table>
                </div>
                <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                    <div class="me-1">
                        <a class="btn btn-outline-secondary" href="{{ route('customer.index') }}">
                            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                            <span>Back</span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /Project table -->
        </div>
        <!--/ User Content -->
    </div>
</section>

@endsection

@section('vendor-script')
    <script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
      {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  {{-- data table --}}
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view-account.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
@endsection

