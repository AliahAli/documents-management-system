@extends('layouts/contentLayoutMaster')

@section('title', 'Edit Sidebar')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')
<section>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Sidebar</span></h4>
        </div>
    </div>
    <div class="card">
        <div class="card-body px-2 py-2 align-items-center">
            <form action="{{ route('sidebar.update', $sidebar->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for="name">Name :</h4>
                                    <input type="text" class="form-control" id="name" name="name" id="readonlyInput" readonly="readonly" value="{{ $sidebar->name }}" />
                                </div>
                            </div>
                            <div class="col-12 mt-1">
                                <h4 class="card-title" for="value">Value :</h4>
                                <textarea class="form-control" rows="1" id="value" name="value">{{ old('value', $sidebar->value) }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mt-1">
                            <div class="mb-1">
                                <h4 class="card-title" for="name">Image Name :</h4>
                                <input type="text" class="form-control" id="image" name="image" value="{{ $sidebar->image }}" />
                                <p>Image name will shown beside your image at side menu</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mt-1">
                            <h4 class="card-title" for="name">Image :</h4>
                            <input class="form-control" type="file" id="path" name="path" />
                        </div>
                        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                            <div class="me-1">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection