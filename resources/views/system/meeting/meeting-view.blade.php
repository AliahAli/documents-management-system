@extends('layouts/contentLayoutMaster')

@section('title', 'View Meeting')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice.css')}}">
@endsection

@section('content')
<section class="invoice-preview-wrapper">
    <div class="row invoice-preview">
        <!-- Invoice -->
        <div class="col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding pb-0">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div class="col-12">
                            <h6 class="mb-2">Meeting Details:</h6>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="pe-1">Date</td>
                                        <td><span class="fw-bold">: {{ Carbon\Carbon::parse($meeting->meeting_at)->format('D- d M Y') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Location</td>
                                        <td><span class="fw-bold">: {{ $meeting->location}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Members</td>
                                        <td><span class="fw-bold">: {{ $meeting->member }}</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>
                <hr class="invoice-spacing" />
                <!-- Invoice Note starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row">
                        <div class="col-12">
                            <span class="fw-bold">Meeting Update:</span>
                            <span>{{ $meeting->meeting_update }}</span>
                        </div>
                    </div>
                </div>
                <!-- Invoice Note ends -->
                <hr class="invoice-spacing" />
                <div class="card-body invoice-padding pb-0 mb-2">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div class="col-12">
                            <h6 class="mb-2">Document Details:</h6>
                            <table>
                                <tbody>
                                    @foreach ( $meeting->documents as $document )
                                    <tr>
                                        <td class="pe-1">Title</td>
                                        <td><span class="fw-bold">: {{ $document->title }}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Description</td>
                                        <td><span class="fw-bold">: {{ $document->description }}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">File</td>
                                        <td><span class="fw-bold">: {{ $document->filename }}</span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>
                <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                    <div class="me-1">
                        <a class="btn btn-outline-secondary" href="{{ route('customer.overview', $customer) }}">
                            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                            <span>Back</span>
                        </a>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /Invoice -->
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{asset('vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice.js')}}"></script>
@endsection
