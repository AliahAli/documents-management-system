@extends('layouts/contentLayoutMaster')

@section('title', 'Create Meeting')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha256-aAr2Zpq8MZ+YA/D6JtRD3xtrwpEz2IqOS+pWD/7XKIw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <style type="text/css">
        .label-info{
            background-color: #17a2b8;

        }
        .label {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,
            border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
    </style>
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <style>
        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<section>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Create Meeting for <span>{{ $customer->company_name}}</span></h4>
        </div>
    </div>
    <div class="card bg-light-primary">
        <ul class="list-unstyled px-2 py-2">
            <li>
                <span class="fw-bolder">Company Name: </span>
                <span> {{ $customer->company_name}}</span>
            </li>
            <li class="mt-1">
                <span class="fw-bolder">Address: </span>
                <span> {{$customer->address1}} , {{$customer->address1}} , {{$customer->postcode}} , {{$customer->city}} , {{$customer->state}} </span>
            </li>
            <li class="mt-1">
                <span class="fw-bolder">Website: </span>
                <span> {{$customer->website}}</span>
            </li>
        </ul>
    </div>
    <div class="card">
        <div class="card-body px-2 py-2 align-items-center">
            <form method="POST" class="repeater-default" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <input type="hidden" class="form-control" rows="5" id="user_id" name="user_id" value="{{ auth()->user()->id}}" required />
                            <div class="col-12 mb-1">
                                <h4 class="card-title" for="">Meeting Update :</h4>
                                <textarea class="form-control" rows="5" id="meeting_update" name="meeting_update" required></textarea>
                            </div>
                            <div class="col-md-6 mb-1">
                                <h4 class="card-title" for="">Date :</h4>
                                <input type="text" id="meeting_at" name="meeting_at" class="form-control flatpickr-human-friendly" placeholder="Select date" required />
                            </div>
                            <div class="col-xl-6 col-md-6 col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for="">Location :</h4>
                                    <input type="text" class="form-control" id="location" name="location" placeholder="Meeting location" required />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <h4 class="card-title" for="">Team members:</h4>
                                    <textarea type="text" class="form-control" id="member" name="member" required></textarea>
                                </div>
                            </div>
                            <!-- Invoice repeater -->
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Documents :</h4>
                                    </div>
                                    <div class="card-body">
                                        <div data-repeater-list="documents">
                                            <div data-repeater-item>
                                                <div class="row d-flex align-items-end">
                                                    <div class="row">
                                                        <div class="col-12 mb-1">
                                                            <label class="form-label" for="itemname">Title </label>
                                                            <input type="text" class="form-control" id="title" name="documents[][title][]" required />
                                                        </div>
                                                        <div class="col-md-6 col-12">
                                                            <label class="form-label" for="itemquantity">Brief Description</label>
                                                            <textarea class="form-control" rows="3" id="description" name="documents[][description][]" required></textarea>
                                                        </div>
                                                        <div class="col-md-6 col-12">
                                                            <p class="form-label">File (Click button or drop your file here)</p>
                                                            <input class="form-control-file dropzone" type="file" id="filename" name="documents[][filename][]" required />
                                                        </div>
                                                        <div class="col-12 mb-2 mt-1">
                                                            <label class="form-label" for="itemquantity">Meta tags</label>
                                                            <input type="text" data-role="tagsinput" name="tag" class="form-control tags" name="documents[][tag][]" required>
                                                        </div>
                                                        <div class="col-md-2 col-12 mb-50 ">
                                                            <div class="mb-1">
                                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                    <i data-feather="x" class="me-25"></i>
                                                                    <span>Delete</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>Add New</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Invoice repeater -->
                        </div>
                        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                            <div class="me-1">
                                <a class="btn btn-outline-secondary" href="{{ route('customer.overview', $customer) }}">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span>Back</span>
                                </a>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-tagsinput.js')) }}" ></script>
  <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
  <script>
        
  </script>
@endsection
