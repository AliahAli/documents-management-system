
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard Ecommerce')

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
  {{-- Page css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
<!-- Dashboard Ecommerce Starts -->
<section id="dashboard-ecommerce">
    <div class="row match-height">
        <!-- Developer Meetup Card -->
        <div class="col-lg-6 col-12">
            <div class="card card-developer-meetup">
                <div class="meetup-img-wrapper rounded-top text-center">
                    <img src="{{asset('images/illustration/api.svg')}}" alt="Meeting Pic" height="170" />
                </div>
                <div class="card-body">
                    <div class="meetup-header d-flex align-items-center">
                        <div class="meetup-day">
                            <h6 class="mb-0">{{ $today->format('l') }}</h6>
                            <h3 class="mb-0">{{ $today->format('d') }}</h3>
                        </div>
                        <div class="my-auto">
                            <h4 class="card-title mb-25">Today's Meeting</h4>
                        </div>
                    </div>
                    <div class="mt-0">
                        <div class="avatar float-start bg-light-primary rounded me-1">
                            <div class="avatar-content">
                                <i data-feather="calendar" class="avatar-icon font-medium-3"></i>
                            </div>
                        </div>
                        <div class="more-info">
                            <h6 class="mb-1">{{ $today->format('l, M d, Y') }}</h6>
                        </div>
                    </div>
                    @foreach ( $todayMeetings as $today )
                    <div class="mt-2">
                        <div class="avatar float-start bg-light-primary rounded me-1">
                            <div class="avatar-content">
                                <i data-feather="bookmark" class="avatar-icon font-medium-3"></i>
                            </div>
                        </div>
                        <div class="more-info">
                            <h6>{{ $today->customer_name }} at {{ $today->location }}</h6>
                        </div>
                    </div>
                    @endforeach

                    @if (count($todayMeetings) == 0)
                    <tr>
                        <td class="border-t px-6 py-4 text-center" colspan="6">You have no meeting today</td>
                    </tr>
                    @endif
                </div>

            </div>
        </div>
        <!--/ Developer Meetup Card -->

        <!-- Developer Meetup Card -->
        <div class="col-lg-6 col-12">
            <div class="card card-developer-meetup">
                <div class="meetup-img-wrapper rounded-top text-center">
                    <img src="{{asset('images/illustration/personalization.svg')}}" alt="Meeting Pic" height="170" />
                </div>
                <div class="card-body">
                    <div class="meetup-header d-flex align-items-center">
                        <div class="meetup-day">
                            <h6 class="mb-0">{{ $tomorrow->format('l') }}</h6>
                            <h3 class="mb-0">{{ $tomorrow->format('d') }}</h3>
                        </div>
                        <div class="my-auto">
                            <h4 class="card-title mb-25">Tomorrow's Meeting</h4>
                        </div>
                    </div>
                    <div class="mt-0">
                        <div class="avatar float-start bg-light-primary rounded me-1">
                            <div class="avatar-content">
                                <i data-feather="calendar" class="avatar-icon font-medium-3"></i>
                            </div>
                        </div>
                        <div class="more-info">
                            <h6 class="mb-1">{{ $tomorrow->format('l, M d, Y') }}</h6>
                        </div>
                    </div>
                    @foreach ( $tomorrowMeetings as $tomorrow )
                    <div class="mt-2">
                        <div class="avatar float-start bg-light-primary rounded me-1">
                            <div class="avatar-content">
                                <i data-feather="bookmark" class="avatar-icon font-medium-3"></i>
                            </div>
                        </div>
                        <div class="more-info">
                            <h6>{{ $tomorrow->customer_name }} at {{ $tomorrow->location }}</h6>
                        </div>
                    </div>
                    @endforeach

                    @if (count($tomorrowMeetings) == 0)
                    <tr>
                        <td class="border-t px-6 py-4 text-center" colspan="6">You have no meeting tomorrow</td>
                    </tr>
                    @endif
                </div>
            </div>
        </div>
        <!--/ Developer Meetup Card -->
    </div>

    <div class="row match-height">
        <div class="col-lg-4 col-12">
            <!-- User Card -->
            <div class="card">
                <div class="border-bottom">
                    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                        <div class="me-1">
                            <h4 class="card-title">Activities Timeline</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="timeline">
                        @foreach ( $activities as $data )
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{ $data->activity }}</h6>
                                    <span class="timeline-event-time"></span>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- /User Card -->
        </div>
        <!-- Company Table Card -->
        <div class="col-lg-8 col-12">
            <div class="card card-company-table">
                <div class="border-bottom">
                    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                        <div class="me-1">
                            <h4 class="card-title">List of Meetings</h4>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Date</th>
                            <th>Location</th>
                        </tr>
                        @foreach ($meetings as $meeting)
                        <tr class="align-top">
                            <td>{{ $meeting->customer_name }}</td>
                            <td>{{ Carbon\Carbon::parse($meeting->meeting_at)->format('l - d M Y')  }}</td>
                            <td>{{ $meeting->location }}</td>
                        </tr>
                        @endforeach

                        @if (count($meetings) == 0)
                        <tr>
                            <td class="border-t px-6 py-4 text-center" colspan="6">You have no meeting</td>
                        </tr>
                        @endif
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection
