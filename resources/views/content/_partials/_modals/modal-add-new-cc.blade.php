<!-- add new card modal  -->
<div class="modal fade" id="addNewCard" tabindex="-1" aria-labelledby="addNewCardTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body px-sm-5 mx-50 pb-5">
        <h1 class="text-center mb-1" id="addNewCardTitle">New Folder</h1>

        <form action="{{ auth()->user() }}" method="POST"  class="row gy-1 gx-2 mt-75" onsubmit="return false">
          @csrf
          <div class="col-12">
            <label class="form-label" for="modalAddCardName">Folder Name</label>
            <input type="text" id="folderName" class="form-control" name="folderName" value="Untitled Folder" />
          </div>

          <div class="col-12 text-center">
            <button type="submit" class="btn btn-primary me-1 mt-1">Create</button>
            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal" aria-label="Close">
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--/ add new card modal  -->
